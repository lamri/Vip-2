var champRecherche, listeDomainesAcceptés, listeDomainesRefusés, ajouterDomaine, supprimerDomaine, domainesAcceptés, domainesRefusés, ongletcourant, nbrefus,
    journalisationBtn, journalisationActive = true,
    elasticPrefsBtn, préférencesElasticActivées = true,
    elasticStatsBtn, statsElasticActivées = true,
    webRTCBtn, webRTCActivé = false;

function autoriserUnDomainePourLaPage(mouseEvent) {
    var hôte = mouseEvent.target.id.substr(0, mouseEvent.target.id.length - 7 /*taille du suffixe _bouton*/ );
    document.getElementById('rechercheDomaines').value=hôte;
    document.getElementById('ajouter').click();
}

function créerNoeudAjout(parentNode, hoteReprésenté) {
    var div = document.createElement('div');
    div.setAttribute('id', hoteReprésenté);

    var input = document.createElement('input');
    input.setAttribute('type', 'button');
    input.setAttribute('value', '+');
    input.setAttribute('id', hoteReprésenté+'_bouton');
    input.addEventListener("click", autoriserUnDomainePourLaPage);
    input.setAttribute('class', 'ui-button bleu');
    div.appendChild(input);
    div.appendChild(document.createTextNode(' ' + hoteReprésenté));
    parentNode.appendChild(div);
}

function bannirUnDomainePourLaPage(mouseEvent) {

    var hôte = mouseEvent.target.id.substr(0, mouseEvent.target.id.length - 7 /*taille du suffixe _bouton*/ );
    document.getElementById('rechercheDomaines').value=hôte;
    document.getElementById('supprimer').click();
}

function créerNoeudSuppression(parentNode, hoteReprésenté) {

    var div = document.createElement('div');
    div.setAttribute('id', hoteReprésenté);

    var input = document.createElement('input');
    input.setAttribute('type', 'button');
    input.setAttribute('value', '-');
    input.setAttribute('id',hoteReprésenté+'_bouton');
    input.addEventListener("click", bannirUnDomainePourLaPage);
    input.setAttribute('class', 'ui-button bleu');
    div.appendChild(input);
    div.appendChild(document.createTextNode(' ' + hoteReprésenté));

    parentNode.appendChild(div);
}

function nettoyerLesListes() {
    while (domainesAcceptés.hasChildNodes()) {
        domainesAcceptés.removeChild(domainesAcceptés.lastChild);
    }

    while (domainesRefusés.hasChildNodes()) {
        domainesRefusés.removeChild(domainesRefusés.lastChild);
    }
}

function activerDesactiverjournalisation(){
    if (journalisationActive === true) {
        journalisationLabel.innerHTML = 'Journalisation active';
    } else {
        journalisationLabel.innerHTML = 'Journalisation inactive';
    }
}

function activerDesactiverElasticPrefs(){
    if (préférencesElasticActivées === true) {
        elasticPrefsLabel.innerHTML = 'Préférences Elasticsearch actives';
    } else {
        elasticPrefsLabel.innerHTML = 'Préférences Elasticsearch inactives';
    }
}

function activerDesactiverElasticStats(){
    if (statsElasticActivées === true) {
        elasticStatsLabel.innerHTML = 'Statistiques Elasticsearch actives';
    } else {
        elasticStatsLabel.innerHTML = 'Statistiques Elasticsearch inactives';
    }
}

function activerDesactiverWebRTC(){
    if (webRTCActivé === true) {
        webRTCLabel.innerHTML = 'Web RTC actif';
    } else {
        webRTCLabel.innerHTML = 'Web RTC inactif';
    }
}

function listeBlanche(message){
    listeDomainesAcceptés = message.response;
    ongletcourant.innerText = message.hôteCourant;
    nbrefus.innerText = message.refus + ' / ' + message.acceptés;
    journalisationActive = message.journalisation;
    préférencesElasticActivées = message.elasticPrefs;
    statsElasticActivées = message.elasticStats;
    webRTCActivé = message.webRTC;
    activerDesactiverjournalisation();
    activerDesactiverElasticPrefs();
    activerDesactiverElasticStats();
    activerDesactiverWebRTC();
    browser.runtime.sendMessage({msg: 'listeNoire'}).then(listeNoire, erreur);
}

function listeNoire(message){
    listeDomainesRefusés = message.response;
    intialisationAvecListesRemplies();
}

function intialisationAvecListesRemplies(){
    nettoyerLesListes();
    for (var indexDomaine in listeDomainesAcceptés) {
        créerNoeudSuppression(domainesAcceptés, listeDomainesAcceptés[indexDomaine].hôte);
    }
    for (var indexDomaineBanni in listeDomainesRefusés) {
        créerNoeudAjout(domainesRefusés, listeDomainesRefusés[indexDomaineBanni].hôte);
    }
}

function erreur(erreur) {
    console.error('Erreur:', erreur);
}

function init() {
    champRecherche = document.getElementById('rechercheDomaines');
    domainesAcceptés = document.getElementById('domainesAcceptés');
    domainesRefusés = document.getElementById('domainesRefusés');
    ajouterDomaine = document.getElementById('ajouter');
    supprimerDomaine = document.getElementById('supprimer');
    journalisationBtn = document.getElementById('journalisationBtn');
    journalisationLabel = document.getElementById('journalisationLabel');
    elasticPrefsBtn = document.getElementById('elasticPrefsBtn');
    elasticPrefsLabel = document.getElementById('elasticPrefsLabel');
    elasticStatsBtn = document.getElementById('elasticStatsBtn');
    elasticStatsLabel = document.getElementById('elasticStatsLabel');
    webRTCBtn = document.getElementById('webRTCBtn');
    webRTCLabel = document.getElementById('webRTCLabel');
    ongletcourant = document.getElementById('onglet');
    nbrefus = document.getElementById('nbrefus');

    ajouterDomaine.addEventListener('click', function () {
        domainesRefusés.removeChild(document.getElementById(champRecherche.value));
        créerNoeudSuppression(domainesAcceptés, champRecherche.value);
        champRecherche.focus();
        browser.runtime.sendMessage({msg: 'hôteAutoriséPourLaPage', hôte: champRecherche.value}).then(function (){}, erreur);
    }, false);

    supprimerDomaine.addEventListener('click', function () {
        domainesAcceptés.removeChild(document.getElementById(champRecherche.value));
        créerNoeudAjout(domainesRefusés, champRecherche.value);
        champRecherche.focus();
        browser.runtime.sendMessage({msg: 'hôteBanniPourLaPage', hôte: champRecherche.value}).then(function (){}, erreur);
    }, false);

    journalisationBtn.addEventListener('click', function () {
        if (journalisationActive === true) {
            journalisationActive = false;
        } else {
            journalisationActive = true;
        }
        activerDesactiverjournalisation();
        browser.runtime.sendMessage({msg: 'journalisation', journalisation: journalisationActive}).then(function (){}, erreur);
    }, false);

    elasticPrefsBtn.addEventListener('click', function () {
        if (préférencesElasticActivées === true) {
            préférencesElasticActivées = false;
        } else {
            préférencesElasticActivées = true;
        }
        activerDesactiverElasticPrefs();
        browser.runtime.sendMessage({msg: 'elasticPrefs', elasticPrefs: préférencesElasticActivées}).then(function (){}, erreur);
    }, false);

    elasticStatsBtn.addEventListener('click', function () {
        if (statsElasticActivées === true) {
            statsElasticActivées = false;
        } else {
            statsElasticActivées = true;
        }
        activerDesactiverElasticStats();
        browser.runtime.sendMessage({msg: 'elasticStats', elasticStats: statsElasticActivées}).then(function (){}, erreur);
    }, false);

    webRTCBtn.addEventListener('click', function () {
        if (webRTCActivé === true) {
            webRTCActivé = false;
        } else {
            webRTCActivé = true;
        }
        activerDesactiverWebRTC();
        browser.runtime.sendMessage({msg: 'webRTC', webRTC: webRTCActivé}).then(function (){}, erreur);
    }, false);


    browser.runtime.sendMessage({msg: 'listeBlanche'}).then(listeBlanche, erreur);
}

document.addEventListener("load",  init() , true);
